﻿* Environmental Tech
- diode # Diode
- lightning_rod # Lightning Rod
+ lightning_rod_insulated;Insulated Lightning Rod
- photovoltaic_cell # Photovoltaic Cell
- tool_multiblock_assembler # Assembler

* Minecraft
- armor_stand
- bone
- brown_mushroom;Brown
- clay;Block
#- clay_ball;Item
- cooked_mutton
- cooked_rabbit
- double_plant # Sunflower
- double_plant:1 # Lilac
- double_plant:5 # Peony
- end_bricks # End Stone Bricks
+ end_portal;End Portal
- ender_chest
- fish:1 # Raw Salmon
+ golden_apple:1;Enchanted %name% # Enchanted Golden Apple
- lead
- log2 # Acacia Wood
- melon;Item
- melon_block;Block
- milk_bucket
- mutton # Raw Mutton
- nether_brick;Block
- netherbrick;Item
- planks:4 # Acacia Wood Planks
+ potion:8225;%name% II # Potion of Regeneration II
+ potion:8226;%name% II # Potion of Swiftness II
+ potion:8228;%name% II # Potion of Poison II
+ potion:8229;%name% II # Potion of Healing II
+ potion:8233;%name% II # Potion of Strength II
+ potion:8236;%name% II # Potion of Harming II
+ potion:16417;%name% II # Splash Potion of Regeneration II
+ potion:16418;%name% II # Splash Potion of Swiftness II
+ potion:16420;%name% II # Splash Potion of Poison II
+ potion:16421;%name% II # Splash Potion of Healing II
+ potion:16425;%name% II # Splash Potion of Strength II
+ potion:16428;%name% II # Splash Potion of Harming II
- rabbit # Raw Rabbit
- record_11;;%name% (%info%)
- record_13;;%name% (%info%)
- record_blocks;;%name% (%info%)
- record_cat;;%name% (%info%)
- record_chirp;;%name% (%info%)
- record_far;;%name% (%info%)
- record_mall;;%name% (%info%)
- record_mellohi;;%name% (%info%)
- record_stal;;%name% (%info%)
- record_strad;;%name% (%info%)
- record_wait;;%name% (%info%)
- record_ward;;%name% (%info%)
- red_flower # Poppy
- red_flower:2 # Allium
- red_mushroom;Red
- sapling:4 # Acacia Sapling
- sponge
- stone_button;Stone
- stone_pressure_plate;Stone
- yellow_flower
- water_bucket
- wooden_button;Wood
- wooden_pressure_plate;Wood

* Mob Grinding Utils
- fan_upgrade;Width # Mob Fan Upgrade
- fan_upgrade:1;Height # Mob Fan Upgrade
- fan_upgrade:2;Distance # Mob Fan Upgrade
- saw_upgrade;Sharpness # Mob Masher Upgrade
- saw_upgrade:1;Looting # Mob Masher Upgrade
- saw_upgrade:2;Fire Aspect # Mob Masher Upgrade
- saw_upgrade:3;Smite # Mob Masher Upgrade
- saw_upgrade:4;Bane of Arthropods # Mob Masher Upgrade
- saw_upgrade:5;Beheading # Mob Masher Upgrade
- spikes # Iron Spikes

* Sky Resources 2
- alchemyitemcomponent:6 # Alchemical Coal
- baseitemcomponent:3 # Dark Matter
- baseitemcomponent:5 # Sawdust
- cactusfruit # Cactus Fruit
- casing:3 # Iron Casing
- casing:4 # Steel Casing
- coalinfusedblock # Alchemical Coal Block
- crucible # Crucible
- rockcrusher # Rock Crusher
- techitemcomponent:3 # Crushed Netherrack

